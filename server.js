const express = require('express');
const path = require('path');


const app = express();
const server = require('http').createServer(app) //definido protocolo htt
const io = require('socket.io')(server) //definindo protocolo wss 
app.use(express.static(path.join(__dirname,'public'))) //definindo pasta com arquivos publicos acessados pela aplicação
app.set('views',path.join(__dirname,'public')) //definindo pasta das views
app.engine('html',require('ejs').renderFile);
app.set('view engine','html');//definindo engine

app.use('/',(req,res)=>{
    res.render('index.html')
})

let messages = [];
io.on('connection',socket =>{
    console.log(`Socket connectado ${socket.id}`);
    socket.emit('previousMessages',messages)
    socket.on('sendMessage',data =>{
        console.log(data)
        messages.push(data)
        socket.broadcast.emit('receivedMessage',data)
    })
})
server.listen(3000)